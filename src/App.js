// src/App.js
import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import AddTaskForm from './components/AddTaskForm';
import TaskList from './components/TaskList';
import FilterTasks from './components/FilterTasks';
import './App.css'; // Importez le fichier CSS

function App() {
  return (
    <Provider store={store}>
      <div className="container">
        <h1>Task Manager</h1>
        <AddTaskForm />
        <FilterTasks />
        <TaskList />
      </div>
    </Provider>
  );
}

export default App;
