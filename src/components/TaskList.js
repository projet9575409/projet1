// src/components/TaskList.js
import React from 'react';
import { connect } from 'react-redux';
import { toggleTask, deleteTask } from '../actions/taskActions';

const TaskList = ({ tasks, filter, toggleTask, deleteTask }) => {
  const filteredTasks = tasks.filter((task) =>
    filter === 'completed' ? task.completed : filter === 'uncompleted' ? !task.completed : true
  );

  return (
    <ul>
      {filteredTasks.map((task) => (
        <li key={task.id}>
          <span
            style={{ textDecoration: task.completed ? 'line-through' : 'none' }}
            onClick={() => toggleTask(task.id)}
          >
            {task.text}
          </span>
          <button onClick={() => deleteTask(task.id)}>Delete</button>
        </li>
      ))}
    </ul>
  );
};

const mapStateToProps = (state) => ({
  tasks: state.tasks,
  filter: state.filter,
});

export default connect(mapStateToProps, { toggleTask, deleteTask })(TaskList);
