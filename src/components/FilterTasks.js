// src/components/FilterTasks.js
import React from 'react';
import { connect } from 'react-redux';
import { setFilter } from '../actions/taskActions';

const FilterTasks = ({ setFilter }) => {
  return (
    <div>
      <label>
        <input
          type="radio"
          name="filter"
          value="all"
          defaultChecked
          onChange={() => setFilter('all')}
        />
        All
      </label>
      <label>
        <input type="radio" name="filter" value="completed" onChange={() => setFilter('completed')} />
        Completed
      </label>
      <label>
        <input
          type="radio"
          name="filter"
          value="uncompleted"
          onChange={() => setFilter('uncompleted')}
        />
        Uncompleted
      </label>
    </div>
  );
};

export default connect(null, { setFilter })(FilterTasks);
